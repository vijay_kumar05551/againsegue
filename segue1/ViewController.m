//
//  ViewController.m
//  segue1
//
//  Created by clicklabs124 on 11/9/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *inputTextField;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end

@implementation ViewController
@synthesize searchBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if([string isEqualToString:@"1"])
    {
        [self performSegueWithIdentifier:@"go1" sender:nil];
        
    }
    else if([string isEqualToString:@"2"])
    {
        [self performSegueWithIdentifier:@"go2" sender:nil];
    }
    else if ([string isEqualToString:@"3"])
    {
        [self performSegueWithIdentifier:@"go3" sender:nil];
    }
    return YES;
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"1"])
    {
        [self performSegueWithIdentifier:@"go1" sender:nil];
        
    }
    else if([text isEqualToString:@"2"])
    {
        [self performSegueWithIdentifier:@"go2" sender:nil];
    }
    else if ([text isEqualToString:@"3"])
    {
        [self performSegueWithIdentifier:@"go3" sender:nil];
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
